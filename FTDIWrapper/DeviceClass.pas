//Copyright (C) 2013 by Jeroen Vandezande
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

namespace Essy.FTDIWrapper;

interface

uses
  System,
  System.Text, 
  System.Threading, 
  System.Threading.Tasks, 
  System.Threading.Tasks;

type

  FTDI_Device = public class(IDisposable)
  private  
    fDisposed: Boolean := False;
    fDeviceInfo: FTDI_DeviceInfo;
    fWaitForDataEventReady: Boolean := False;
    fWaitDataHandle: EventWaitHandle;
    fWaitForpinChangeEventReady: Boolean := False;
    fWaitPinChangeHandle: EventWaitHandle;
    fDeviceHandle: Handle;
    fLastDataLength: DataLength; 
    fLastParity: Parity; 
    fLastStopBits: StopBits;
    fLastBaudRate: Uint32;
    fLastFlowControl: FlowControl;
    fIsRunningOnLinux: Boolean := (Environment.OSVersion.Platform = PlatformID.Unix);
    fAbortDataAvailablePolling: Boolean;
    fAbortPinChangePolling: Boolean;
    fPrevPinState: StatusPins;
    fDoPinChangePollingDone: future;
    fDoDataAvailablePollingDone: future;
    method fCreateDevice; unsafe;
    method fDoPinChangePolling; async;
    method fDoDataAvailablePolling; async;
  protected
    method Dispose(disposing: Boolean); virtual; 
  public
    method GetSerialNumber: String;
    method ReadBytes(aNumberOfBytes: Int32): Array of Byte; unsafe;
    method WriteBytes(aDataToSend: Array of Byte); unsafe;
    method NumberOfBytesInReceiveBuffer: UInt32; unsafe;
    method SetTimeOuts(aReadTimeout, aWriteTimeOut: UInt32);
    method ClearReceiveBuffer;
    method ClearSendBuffer;
    method WriteToUAEEProm(aText: String); unsafe;
    method WriteToUAEEProm(aData: array of Byte); unsafe;
    method ReadFromUAEEProm: String; unsafe;
    method ReadBinaryFromUAEEProm(aSize: UInt32): array of Byte; unsafe;
    method GetSizeOfUAEEProm: UInt32; unsafe;
    method WaitForData(aTimeOut: Int32): Boolean; unsafe;
    method WaitForData: Boolean;
    method EnableWaitForDataEvent; unsafe;
    method DisableWaitForDataEvent; unsafe;
    method SignalDataWaitEvent; unsafe;
    method WaitForPinChange(aTimeOut: Int32): Boolean; unsafe;
    method EnableWaitForPinChangeEvent; unsafe;
    method DisableWaitForPinChangeEvent; unsafe;
    method SignalPinChangeEvent; unsafe;
    method SetBufferSizes(aSendBuffersize, aReceiveBufferSize: UInt32); unsafe;
    method ResetPort; unsafe;
    method TryResetPort: Boolean; unsafe;
    method SetResetPipeRetryCount(aRetryCount: UInt32); unsafe;
    method SetBufferFlushTimer(aTime: Byte); unsafe;  
    method Reset; unsafe;
    method TryReset: Boolean; unsafe;
    method CyclePort; unsafe;
    method TryCyclePort: Boolean; unsafe;
    method SetBaudrate(aBaudRate: Uint32); unsafe;
    method SetParameters(aDataLength: DataLength; aParity: Parity; aStopBits: StopBits);
    method SetFlowControl(aFlowControl: FlowControl);
    method GetStatusPins: StatusPins; unsafe;
    method SetRTS(aState: Boolean); unsafe;
    method GetDriverVersion: Version; unsafe;
    method Close;
    method Dispose; 
    method TryReloadDevice: Boolean;
    method GetDeviceProductID: UInt16; unsafe;
    method SetDeviceProductID(aPID: UInt16); unsafe;
    constructor(aDeviceInfo: FTDI_DeviceInfo);
    finalizer;
  end;

  Parity = public enum(None = FTDIFunctions.FT_PARITY_NONE, Odd = FTDIFunctions.FT_PARITY_ODD, Even = FTDIFunctions.FT_PARITY_EVEN, Mark = FTDIFunctions.FT_PARITY_MARK, Space = FTDIFunctions.FT_PARITY_SPACE) of Byte;
  
  StopBits = public enum(OneStopBit = FTDIFunctions.FT_STOP_BITS_1, TwoStopBits = FTDIFunctions.FT_STOP_BITS_2) of Byte;

  DataLength = public enum(SevenBits = 7, EightBits = 8) of Byte;

  StatusPins = public flags(CTS = $10, DSR = $20, RI = $40, DCD = $80);

  FlowControl = public enum(None = FTDIFunctions.FT_FLOW_NONE, RTS_CTS = FTDIFunctions.FT_FLOW_RTS_CTS, DTR_DSR = FTDIFunctions.FT_FLOW_DTR_DSR, XON_XOFF = FTDIFunctions.FT_FLOW_XON_XOFF);
  
implementation

method FTDI_Device.ReadBytes(aNumberOfBytes: Int32): Array of Byte; 
begin
  result := new Byte[aNumberOfBytes];
  var bytesRead: UInt32;
  var tempStatus := FTDIFunctions.FT_Read(fDeviceHandle, result, aNumberOfBytes, var bytesRead); 
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not read data.', tempStatus);
  end; 
  if bytesRead <> aNumberOfBytes then
  begin
    raise new FTDIDeviceNotAllBytesTransmittedException('Not all data was read: ' + bytesRead.ToString + '/' + aNumberOfBytes.ToString, tempStatus);  
  end;
end;

method FTDI_Device.WriteBytes(aDataToSend: Array of Byte); 
begin
  var bytesWritten: UInt32;
  var tempStatus := FTDIFunctions.FT_Write(fDeviceHandle, aDataToSend, UInt32(aDataToSend.Length), var bytesWritten); 
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not send data.', tempStatus);
  end; 
  if bytesWritten <> aDataToSend.Length then
  begin
    raise new FTDIDeviceNotAllBytesTransmittedException('Not all data was sent: ' + bytesWritten.ToString + '/' + aDataToSend.Length.ToString, tempStatus);  
  end;
end;

method FTDI_Device.Dispose; 
begin
  Dispose(True);
  // Take yourself off the Finalization queue 
  // to prevent finalization code for this object
  // from executing a second time.
  GC.SuppressFinalize(self);
end;

constructor FTDI_Device(aDeviceInfo: FTDI_DeviceInfo); 
begin
  fDeviceInfo := aDeviceInfo;
  fCreateDevice();  
end;

method FTDI_Device.Close; 
begin
  var tempStatus := FTDIFunctions.FT_Close(fDeviceHandle);
  fDeviceHandle := IntPtr.Zero; 
  fDeviceInfo.DeviceOpened := False;
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not close device.', tempStatus);
  end;
end;

method FTDI_Device.NumberOfBytesInReceiveBuffer: UInt32; 
begin
  var tempStatus := FTDIFunctions.FT_GetQueueStatus(fDeviceHandle, var result);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not get receive buffer size.', tempStatus);
  end; 
end;

method FTDI_Device.ClearReceiveBuffer; 
begin
  var tempStatus := FTDIFunctions.FT_Purge(fDeviceHandle, FTDIFunctions.FT_PURGE_RX);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not clear receive buffer.', tempStatus);
  end; 
end;

method FTDI_Device.ClearSendBuffer; 
begin
  var tempStatus := FTDIFunctions.FT_Purge(fDeviceHandle, FTDIFunctions.FT_PURGE_TX);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not clear send buffer.', tempStatus);
  end; 
end;

method FTDI_Device.SetTimeOuts(aReadTimeout, aWriteTimeOut: UInt32); 
begin
  var tempStatus := FTDIFunctions.FT_SetTimeouts(fDeviceHandle, aReadTimeout, aWriteTimeOut);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not set timeouts.', tempStatus);
  end; 
end;

method FTDI_Device.WriteToUAEEProm(aText: String); 
begin
  var tempStatus := FTDIFunctions.FT_EE_UAWrite(fDeviceHandle, aText, aText.Length);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not write to UAEEProm.', tempStatus);
  end;
end;

method FTDI_Device.GetSizeOfUAEEProm: UInt32; 
begin
  var tempStatus := FTDIFunctions.FT_EE_UASize(fDeviceHandle, var result);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not get UAEEProm size.', tempStatus);
  end;
end;

method FTDI_Device.ReadFromUAEEProm: String; 
begin
  var tempLength: UInt32;
  var size := GetSizeOfUAEEProm();
  var tempText: String;
  var tempStatus := FTDIFunctions.FT_EE_UARead(fDeviceHandle, tempText, size, var tempLength);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not read from UAEEProm.', tempStatus);
  end;
  result := tempText.ToString;
  if (result = '') or (result = #1) then result := 'Unnamed Instrument';
end;

method FTDI_Device.GetSerialNumber: String; 
begin
  result := fDeviceInfo.DeviceSerialNumber;
end;

method FTDI_Device.WaitForData(aTimeOut: Int32): Boolean;
require
  fWaitForDataEventReady
begin
  result := fWaitDataHandle.WaitOne(aTimeOut);
end;

method FTDI_Device.EnableWaitForDataEvent;
begin
  fWaitDataHandle := new EventWaitHandle(false, EventResetMode.AutoReset);
  if fIsRunningOnLinux then
  begin
    fDoDataAvailablePollingDone := fDoDataAvailablePolling();
  end
  else 
  begin   
    var eventMask: UInt32 := FTDIFunctions.FT_EVENT_RXCHAR;
    var tempStatus := FTDIFunctions.FT_SetEventNotification(fDeviceHandle, eventMask, fWaitDataHandle.SafeWaitHandle);
    if tempStatus <> FT_STATUS.OK then raise new FTDIException('Could not init event notification', tempStatus);
    fWaitForDataEventReady := true;
  end; 
end;

method FTDI_Device.DisableWaitForDataEvent;
begin
  if not fWaitForDataEventReady then exit;
  if fIsRunningOnLinux then
  begin
    fAbortDataAvailablePolling := true;
    fDoDataAvailablePollingDone();
    fAbortDataAvailablePolling := false;
  end;
  disposeAndNil(fWaitDataHandle);
  fWaitForDataEventReady := false;
end;

method FTDI_Device.SetBufferSizes(aSendBuffersize, aReceiveBufferSize: UInt32);
begin
  var tempStatus := FTDIFunctions.FT_SetUSBParameters(fDeviceHandle, aReceiveBufferSize, aSendBuffersize);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not set buffersizes: Send:' + aSendBuffersize.ToString + ' Receive: ' + aReceiveBufferSize.ToString , tempStatus);
  end;
end;

method FTDI_Device.SetBufferFlushTimer(aTime: Byte);
begin
  var tempStatus := FTDIFunctions.FT_SetLatencyTimer(fDeviceHandle, aTime);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not set Flushtimer value: ' + aTime.ToString, tempStatus);
  end;
end;

method FTDI_Device.SignalDataWaitEvent;
begin
  fWaitDataHandle.Set();
end;

method FTDI_Device.Reset;
begin
  var tempStatus := FTDIFunctions.FT_ResetDevice(fDeviceHandle);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not reset device' , tempStatus);
  end;
end;

method FTDI_Device.ResetPort;
begin
  if fIsRunningOnLinux then exit;
  var tempStatus := FTDIFunctions.FT_ResetPort(fDeviceHandle);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not reset USB port' , tempStatus);
  end;
end;

method FTDI_Device.SetResetPipeRetryCount(aRetryCount: UInt32);
begin
  if fIsRunningOnLinux then exit;
  var tempStatus := FTDIFunctions.FT_SetResetPipeRetryCount(fDeviceHandle, aRetryCount);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not set ResetPipeRetryCount' , tempStatus);
  end;
end;

method FTDI_Device.CyclePort;
begin
  if fIsRunningOnLinux then exit;
  var tempStatus := FTDIFunctions.FT_CyclePort(fDeviceHandle);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not cycle USB port' , tempStatus);
  end;
end;

method FTDI_Device.Dispose(disposing: Boolean);
begin
  if not fDisposed then
  begin
    self.Close;
    DisableWaitForDataEvent();
    DisableWaitForPinChangeEvent();
  end;
  fDisposed := true;
end;

finalizer FTDI_Device;
begin
  Dispose(false);
end;

method FTDI_Device.SetBaudrate(aBaudRate: Uint32);
begin
  if aBaudRate < 120 then exit;
  var tempStatus := FTDIFunctions.FT_SetBaudrate(fDeviceHandle, aBaudRate);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not set Baudrate value: ' + aBaudrate.ToString, tempStatus);
  end;
  fLastBaudRate := aBaudRate;
  tempStatus := FTDIFunctions.FT_SetDataCharacteristics(fDeviceHandle, 8, FTDIFunctions.FT_STOP_BITS_1, FTDIFunctions.FT_PARITY_NONE);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not set Data Characteristics ', tempStatus);
  end;
  tempStatus := FTDIFunctions.FT_SetFlowControl(fDeviceHandle, FTDIFunctions.FT_FLOW_NONE, 0, 0);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not set Flow Control', tempStatus);
  end;
end;

method FTDI_Device.EnableWaitForPinChangeEvent;
begin
  fWaitPinChangeHandle := new EventWaitHandle(false, EventResetMode.AutoReset);
  if fIsRunningOnLinux then
  begin
    fDoPinChangePollingDone := fDoPinChangePolling();
  end
  else 
  begin
    var eventMask: UInt32 := FTDIFunctions.FT_EVENT_MODEM_STATUS;
    var tempStatus := FTDIFunctions.FT_SetEventNotification(fDeviceHandle, eventMask, fWaitPinChangeHandle.SafeWaitHandle);
    if tempStatus <> FT_STATUS.OK then raise new FTDIException('Could not init event notification', tempStatus);
    fWaitForpinChangeEventReady := true;
  end;
end;

method FTDI_Device.DisableWaitForPinChangeEvent;
begin
  if not fWaitForpinChangeEventReady then exit;
  if fIsRunningOnLinux then
  begin
    fAbortPinChangePolling := true;
    fDoPinChangePollingDone();
    fAbortPinChangePolling := false;
  end;
  disposeAndNil(fWaitPinChangeHandle);
  fWaitForpinChangeEventReady := false;
end;

method FTDI_Device.WaitForPinChange(aTimeOut: Int32): Boolean;
require
  fWaitForpinChangeEventReady;
begin
  result := fWaitPinChangeHandle.WaitOne(aTimeOut);
end;

method FTDI_Device.SignalPinChangeEvent;
begin
  fWaitPinChangeHandle:&Set();
end;

method FTDI_Device.SetParameters(aDataLength: DataLength; aParity: Parity; aStopBits: StopBits);
begin
  if aDataLength < 7 then exit;
  var tempStatus := FTDIFunctions.FT_SetDataCharacteristics(fDeviceHandle, Byte(aDataLength), Byte(aStopBits), Byte(aParity));
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not set Data Characteristics ', tempStatus);
  end;
  fLastDataLength := aDataLength;
  fLastParity := aParity;
  fLastStopBits := aStopBits;
end;

method FTDI_Device.GetStatusPins: StatusPins;
begin
  var sp: UInt32;
  var tempStatus := FTDIFunctions.FT_GetModemStatus(fDeviceHandle, var sp);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not get Status pins.', tempStatus);
  end;
  result := StatusPins(sp);
end;

method FTDI_Device.fCreateDevice;
begin
  var tempSerialNumber := fDeviceInfo.DeviceSerialNumber;
  var tempStatus := FTDIFunctions.FT_OpenEx(tempSerialNumber, FTDIFunctions.FT_OPEN_BY_SERIAL_NUMBER, var fDeviceHandle);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not open device with serial number : ' + fDeviceInfo.DeviceSerialNumber, tempStatus);
  end;
  SetBaudrate(fLastBaudRate);
  SetParameters(fLastDataLength, fLastParity, fLastStopBits);
  SetFlowControl(fLastFlowControl);
  fDeviceInfo.DeviceOpened := true;
end;

method FTDI_Device.TryReloadDevice: Boolean;
begin
  result :=  true;
  FTDIFunctions.FT_Close(fDeviceHandle);
  fDeviceInfo.DeviceOpened := false;
  try
    fCreateDevice();
  except
    on ex: FTDIException do
    begin
      result := false;
    end;
  end;
end;

method FTDI_Device.TryResetPort: Boolean;
begin
  if fIsRunningOnLinux then exit true;
  var tempStatus := FTDIFunctions.FT_ResetPort(fDeviceHandle);
  result := tempStatus = FT_STATUS.OK;
end;

method FTDI_Device.TryReset: Boolean;
begin
  var tempStatus := FTDIFunctions.FT_ResetDevice(fDeviceHandle);
  result := tempStatus = FT_STATUS.OK;
end;

method FTDI_Device.TryCyclePort: Boolean;
begin
  if fIsRunningOnLinux then exit true;
  var tempStatus := FTDIFunctions.FT_CyclePort(fDeviceHandle);
  result := tempStatus = FT_STATUS.OK;
end;

method FTDI_Device.SetRTS(aState: Boolean);
begin
  var tempStatus: FT_STATUS;
  if aState then
    tempStatus := FTDIFunctions.FT_SetRts(fDeviceHandle)
  else
    tempStatus := FTDIFunctions.FT_ClrRts(fDeviceHandle);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not set RTS', tempStatus);
  end;  
end;

method FTDI_Device.GetDriverVersion: Version;
begin
  if fIsRunningOnLinux then exit;
  var ver: UInt32;
  var tempStatus := FTDIFunctions.FT_GetDriverVersion(fDeviceHandle, var ver);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not get driver version.', tempStatus);
  end;
  var verArray := BitConverter.GetBytes(ver);
  result := new Version(verArray[2], verArray[1], verArray[0]);
end;

method FTDI_Device.WaitForData: Boolean;
begin
  result := WaitForData(-1);
end;

method FTDI_Device.ReadBinaryFromUAEEProm(aSize: UInt32): array of Byte;
begin
  var tempLength: UInt32;
  if GetSizeOfUAEEProm() < aSize then raise new FTDIDeviceLowLevelException('Could not read from UAEEProm. (not enough room)');
  result := new Byte[aSize];
  var tempStatus := FTDIFunctions.FT_EE_UARead(fDeviceHandle, result, aSize, var tempLength);
  if (tempStatus <> FT_STATUS.OK) or (tempLength <> aSize) then
  begin
    raise new FTDIDeviceLowLevelException('Could not read from UAEEProm.', tempStatus);
  end;
end;

method FTDI_Device.WriteToUAEEProm(aData: array of Byte);
begin
  var size: Int32 := GetSizeOfUAEEProm;
  if size < aData.Length then raise new FTDIDeviceLowLevelException('Could not write to UAEEProm. (not enough room)');
  var tempStatus := FTDIFunctions.FT_EE_UAWrite(fDeviceHandle, aData, size);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not write to UAEEProm.', tempStatus);
  end;
end;

method FTDI_Device.GetDeviceProductID: UInt16;
begin
  var eepromData := new FT_PROGRAM_DATA;
  var tempStatus := FTDIFunctions.FT_EE_Read(fDeviceHandle, var eepromData);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not read PID', tempStatus);
  end;
  exit eepromData.ProductID;
end;

method FTDI_Device.SetDeviceProductID(aPID: UInt16);
begin
  var usereepromSize := GetSizeOfUAEEProm();
  var usereepromData := new Byte[usereepromSize];
  var bytesRead: UInt32;
  var tempStatus := FTDIFunctions.FT_EE_UARead(fDeviceHandle, usereepromData, usereepromSize, var bytesRead);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not read User EEPROM', tempStatus);
  end;
  var eepromData := new FT_PROGRAM_DATA;
  tempStatus := FTDIFunctions.FT_EE_Read(fDeviceHandle, var eepromData);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not read PID', tempStatus);
  end;
  eepromData.ProductID := aPID;
  tempStatus := FTDIFunctions.FT_EE_Program(fDeviceHandle, var eepromData);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not write PID', tempStatus);
  end;
  tempStatus := FTDIFunctions.FT_EE_UAWrite(fDeviceHandle, usereepromData, usereepromSize);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException('Could not write User EEPROM', tempStatus);
  end;
end;

method FTDI_Device.SetFlowControl(aFlowControl: FlowControl);
begin
  var tempStatus := FTDIFunctions.FT_SetFlowControl(fDeviceHandle, Int32(aFlowControl), 0, 0);
  if tempStatus <> FT_STATUS.OK then
  begin
    raise new FTDIDeviceLowLevelException(String.Format('Could not set Flow Control to {0}', aFlowControl), tempStatus);
  end;
  fLastFlowControl := aFlowControl;
end;

method FTDI_Device.fDoPinChangePolling;
begin
  fWaitForpinChangeEventReady := true;
  repeat
    Thread.Sleep(5);
    var sp := GetStatusPins();
    if sp <> fPrevPinState then
    begin
      fPrevPinState := sp;
      fWaitPinChangeHandle.Set();
    end;
  until fAbortPinChangePolling;
end;

method FTDI_Device.fDoDataAvailablePolling;
begin
  fWaitForDataEventReady := true;
  repeat
    Thread.Sleep(5);
    if NumberOfBytesInReceiveBuffer() > 0 then fWaitDataHandle.Set();
  until fAbortDataAvailablePolling;
end;

end.