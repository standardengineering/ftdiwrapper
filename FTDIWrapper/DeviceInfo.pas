//Copyright (C) 2013 by Jeroen Vandezande
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

namespace Essy.FTDIWrapper;

interface

uses
  System.Collections.Generic,
  System.Text;

type

  FTDI_DeviceInfo = public class
  private
  protected
  public
    property DeviceSerialNumber: String;
    property DeviceDescription: String;
    property DeviceOpened: Boolean;
    property DeviceType: UInt32;
    property DeviceID: UInt32;
    property DeviceLocationID: UInt32;
    class method EnumerateDevices: List<FTDI_DeviceInfo>; unsafe;
    class method EnumerateDevices(aDeviceName: String): List<FTDI_DeviceInfo>; unsafe;
    method ToString: String; override;
  end;

  extension method String.CleanUpNullTermination: String;
  
  
implementation

uses 
  System.Runtime.CompilerServices;

class method FTDI_DeviceInfo.EnumerateDevices: List<FTDI_DeviceInfo>; 
begin
  result := self.EnumerateDevices(nil);
end;

method FTDI_DeviceInfo.ToString: String; 
begin
  var NL := Environment.NewLine;
  result := 'Opened: ' + DeviceOpened.ToString + NL + 
            'Type: ' + DeviceType.ToString + NL +
            'ID: ' + DeviceID.ToString + NL +
            'Location ID: ' + DeviceLocationID.ToString + NL +
            'Serial Number: ' + DeviceSerialNumber + NL +
            'Description: ' + DeviceDescription;
end;

class method FTDI_DeviceInfo.EnumerateDevices(aDeviceName: String): List<FTDI_DeviceInfo>;
var 
  numberOfDevs, tempFlags, tempID, tempType, tempLocID: UInt32;
  tempHandle: IntPtr;
begin
  result := new List<FTDI_DeviceInfo>;
  try
    var tempStatus: FT_STATUS;
    tempStatus := FTDIFunctions.FT_CreateDeviceInfoList(var numberOfDevs);
    if tempStatus <> FT_STATUS.OK then
    begin
      raise new FTDIEnumerationException('FT_CreateDeviceInfoList function Failed.', tempStatus);
    end;
    var tempDeviceInfo: FTDI_DeviceInfo;
    for i: Int32 := 0 to numberofDevs - 1 do
    begin
      var tempSerialNumber := new Byte[16];
      var tempDesc := new Byte[64]; 
      tempStatus := FTDIFunctions.FT_GetDeviceInfoDetail(i, var tempFlags, var tempType, var tempID, var tempLocID, tempSerialNumber, tempDesc, var tempHandle);   
      if tempStatus <> FT_STATUS.OK then
      begin
        raise new FTDIEnumerationException('FT_GetDeviceInfoDetail function Failed.', tempStatus);
      end;
      var tempDescString := Encoding.ASCII.GetString(tempDesc).CleanUpNullTermination();
      var tempSerialNumberString := Encoding.ASCII.GetString(tempSerialNumber).CleanUpNullTermination();
      if (aDeviceName = nil) or (aDeviceName = tempDescString) then
      begin
        tempDeviceInfo := new FTDI_DeviceInfo;
        tempDeviceInfo.DeviceOpened := (tempFlags and 1) = 1; //bit 0 in flags is 1 when device is opened.
        tempDeviceInfo.DeviceType := tempType;
        tempDeviceInfo.DeviceID := tempID;
        tempDeviceInfo.DeviceLocationID := tempLocID;
        tempDeviceInfo.DeviceSerialNumber := tempSerialNumberString;
        tempDeviceInfo.DeviceDescription := tempDescString;
        result.Add(tempDeviceInfo);
      end;
    end;
  except
    on ex: DllNotFoundException do
    begin
      //do nothing and return an empty list
    end;
  end;
end;

extension method String.CleanUpNullTermination: String;
begin
  var indx := self.IndexOf(#0);
  if indx >= 0 then
    exit self.Substring(0, indx)
  else
    exit self;
end;

end.