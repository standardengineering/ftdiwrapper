//Copyright (C) 2013 by Jeroen Vandezande
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

namespace Essy.FTDIWrapper;

interface

uses
  System.Text,
  System.Runtime.InteropServices;

type
  
  Handle = IntPtr;
  ByteArray = Array of Byte;
  
  FTDIFunctions = assembly class
  private
  protected
  public
    const
      FT_LIST_NUMBER_ONLY: UInt32      = $80000000;
      FT_LIST_BY_INDEX: UInt32	       = $40000000;
      FT_LIST_ALL: UInt32			         = $20000000;
      FT_OPEN_BY_SERIAL_NUMBER: UInt32 = 1;
      FT_OPEN_BY_DESCRIPTION: UInt32   = 2;
      FT_PURGE_RX: UInt32              = 1;
      FT_PURGE_TX: UInt32              = 2;
      FT_STOP_BITS_1                   = 0;
      FT_STOP_BITS_2                   = 2;
      FT_PARITY_NONE                   = 0;
      FT_PARITY_ODD                    = 1;
      FT_PARITY_EVEN                   = 2;
      FT_PARITY_MARK                   = 3;
      FT_PARITY_SPACE                  = 4;
      FT_FLOW_NONE                     = $0000;
      FT_FLOW_RTS_CTS                  = $0100;
      FT_FLOW_DTR_DSR                  = $0200;
      FT_FLOW_XON_XOFF                 = $0400;
      FT_EVENT_RXCHAR                  = 1;
      FT_EVENT_MODEM_STATUS            = 2;
      [DllImport('ftd2xx.dll')]
      class method FT_OpenEx(pvArg1: String; dwFlags: UInt32; var ftHandle: Handle): FT_STATUS; external; unsafe;
      [DllImport('ftd2xx.dll')]
      class method FT_Close(ftHandle: Handle): FT_STATUS; external; unsafe;
      [DllImport('ftd2xx.dll')]
      class method FT_Read(ftHandle: Handle; lpBuffer: ByteArray; dwBytesToRead: UInt32; var lpdwBytesReturned: UInt32): FT_STATUS; external; unsafe;
      [DllImport('ftd2xx.dll')]
      class method FT_Write(ftHandle: Handle; lpBuffer: ByteArray; dwBytesToWrite: UInt32; var lpdwBytesWritten: UInt32): FT_STATUS; external; unsafe;
      [DllImport('ftd2xx.dll')]
      class method FT_Purge(ftHandle: Handle; dwMask: UInt32): FT_STATUS; external; unsafe;
      [DllImport('ftd2xx.dll')]
      class method FT_SetTimeouts(ftHandle: Handle; dwReadTimeout: UInt32; dwWriteTimeout: UInt32): FT_STATUS; external; unsafe;
      [DllImport('ftd2xx.dll')]
      class method FT_GetQueueStatus(ftHandle: Handle; var lpdwAmountInRxQueue: UInt32): FT_STATUS; external; unsafe;
      [DllImport('ftd2xx.dll')]
      class method FT_CreateDeviceInfoList(var lpdwNumDevs: UInt32): FT_STATUS; external; unsafe;
      [DllImport('ftd2xx.dll')]
      class method FT_GetDeviceInfoDetail(dwIndex: UInt32; var lpdwFlags: UInt32; var lpdwType: UInt32; var lpdwID: UInt32; var lpdwLocId: UInt32; pcSerialNumber, pcDescription: ByteArray; var ftHandle: Handle): FT_STATUS; external; unsafe;
      [DllImport('ftd2xx.dll')]
      class method FT_EE_Read(ftHandle: Handle; var pData: FT_PROGRAM_DATA): FT_STATUS; external; unsafe;
      [DllImport('ftd2xx.dll')]
      class method FT_EE_Program(ftHandle: Handle; var pData: FT_PROGRAM_DATA): FT_STATUS; external; unsafe;
      [DllImport('ftd2xx.dll')]
      class method FT_EE_UARead(ftHandle: Handle; pucData: String; dwDataLen: UInt32; var lpdwBytesRead: UInt32): FT_STATUS; external; unsafe;   
      [DllImport('ftd2xx.dll')]
      class method FT_EE_UAWrite(ftHandle: Handle; pucData: String; dwDataLen: UInt32): FT_STATUS; external; unsafe;  
      [DllImport('ftd2xx.dll')]
      class method FT_EE_UARead(ftHandle: Handle; lpBuffer: ByteArray; dwDataLen: UInt32; var lpdwBytesRead: UInt32): FT_STATUS; external; unsafe;   
      [DllImport('ftd2xx.dll')]
      class method FT_EE_UAWrite(ftHandle: Handle; lpBuffer: ByteArray; dwDataLen: UInt32): FT_STATUS; external; unsafe;  
      [DllImport('ftd2xx.dll')]
      class method FT_EE_UASize(ftHandle: Handle; var lpdwSize: UInt32): FT_STATUS; external; unsafe;  
      [DllImport('ftd2xx.dll')]
      class method FT_SetEventNotification(ftHandle: Handle; dwEventMask: UInt32; pvArg: SafeHandle): FT_STATUS; external; unsafe;  
      [DllImport('ftd2xx.dll')]
      class method FT_SetUSBParameters(ftHandle: Handle; dwInTransferSize, dwOutTransferSize: UInt32): FT_STATUS; external; unsafe;   
      [DllImport('ftd2xx.dll')]
      class method FT_SetLatencyTimer(ftHandle: Handle; ucTimer: Byte): FT_STATUS; external; unsafe;   
      [DllImport('ftd2xx.dll')]
      class method FT_ResetDevice(ftHandle: Handle):FT_STATUS; external; unsafe;   
      [DllImport('ftd2xx.dll')]
      class method FT_ResetPort(ftHandle: Handle):FT_STATUS; external; unsafe;   
      [DllImport('ftd2xx.dll')]
      class method FT_SetResetPipeRetryCount(ftHandle: Handle; dwCount: UInt32):FT_STATUS; external; unsafe;   
      [DllImport('ftd2xx.dll')]
      class method FT_CyclePort(ftHandle: Handle):FT_STATUS; external; unsafe;   
      [DllImport('ftd2xx.dll')]
      class method FT_SetBaudRate(ftHandle: Handle; dwBaudRate: Uint32): FT_STATUS; external; unsafe;  
      [DllImport('ftd2xx.dll')]
      class method FT_SetDataCharacteristics(ftHandle: Handle; uWordLength, uStopBits, uParity: Byte): FT_STATUS; external; unsafe;  
      [DllImport('ftd2xx.dll')]
      class method FT_SetFlowControl(ftHandle: Handle; usFlowControl: UInt16; uXon, uXoff: Byte): FT_STATUS;  external; unsafe;  
      [DllImport('ftd2xx.dll')]
      class method FT_GetModemStatus(ftHandle: Handle; var lpdwModemStatus: UInt32): FT_STATUS; external; unsafe; 
      [DllImport('ftd2xx.dll')]
      class method FT_SetRts(ftHandle: Handle): FT_STATUS; external; unsafe;  
      [DllImport('ftd2xx.dll')]
      class method FT_ClrRts(ftHandle: Handle): FT_STATUS; external; unsafe; 
      [DllImport('ftd2xx.dll')]
      class method FT_GetDriverVersion(ftHandle: Handle; var lpdwDriverVersion: UInt32): FT_STATUS; external; unsafe;  
  end;
 
  [StructLayout(LayoutKind.Sequential)]
  SECURITY_ATTRIBUTES = assembly record
    nLength: Integer;
    lpSecurityDescriptor: Integer;
    bInheritHandle: Integer;
  end;

 FT_STATUS = public enum(
			OK = 0,
			INVALID_HANDLE,
			DEVICE_NOT_FOUND,
			DEVICE_NOT_OPENED,
			IO_ERROR,
			INSUFFICIENT_RESOURCES,
			INVALID_PARAMETER,
			INVALID_BAUD_RATE,
			DEVICE_NOT_OPENED_FOR_ERASE,
			DEVICE_NOT_OPENED_FOR_WRITE,
			FAILED_TO_WRITE_DEVICE,
			EEPROM_READ_FAILED,
			EEPROM_WRITE_FAILED,
			EEPROM_ERASE_FAILED,
			EEPROM_NOT_PRESENT,
			EEPROM_NOT_PROGRAMMED,
			INVALID_ARGS,
			OTHER_ERROR);  

  [StructLayout(LayoutKind.Sequential)]
  FT_PROGRAM_DATA = assembly record
  public
    var Signature1: LongWord := 0;
    var Signature2: LongWord := $FFFFFFFF;
    var Version: LongWord := 4;
    var VendorID: Word := $0403;
    var ProductID: Word;
    var Manufacturer: Intptr := Marshal.AllocHGlobal(32);
    var ManufacturerID: Intptr := Marshal.AllocHGlobal(16);
    var Description: Intptr := Marshal.AllocHGlobal(64);
    var SerialNumber: Intptr := Marshal.AllocHGlobal(16);
    var MaxPower: Word;
    var PnP: Word;
    var SelfPowered: Word;
    var RemoteWakeup: Word;
    var Rev4: Byte;
    var IsoIn: Byte;
    var IsoOut: Byte;
    var PullDownEnable: Byte;
    var SerNumEnable: Byte;
    var USBVersionEnable: Byte;
    var USBVersion: Word;
    var Rev5: Byte;
    var IsoInA: Byte;
    var IsoInB: Byte;
    var IsoOutA: Byte;
    var IsoOutB: Byte;
    var PullDownEnable5: Byte;
    var SerNumEnable5: Byte;
    var USBVersionEnable5: Byte;
    var USBVersion5: Word;
    var AIsHighCurrent: Byte;
    var BIsHighCurrent: Byte;
    var IFAIsFifo: Byte;
    var IFAIsFifoTar: Byte;
    var IFAIsFastSer: Byte;
    var AIsVCP: Byte;
    var IFBIsFifo: Byte;
    var IFBIsFifoTar: Byte;
    var IFBIsFastSer: Byte;
    var BIsVCP: Byte;
    var UseExtOsc: Byte;
    var HighDriveIOs: Byte;
    var EndpointSize: Byte;
    var PullDownEnableR: Byte;
    var SerNumEnableR: Byte;
    var InvertTXD: Byte;
    var InvertRXD: Byte;
    var InvertRTS: Byte;
    var InvertCTS: Byte;
    var InvertDTR: Byte;
    var InvertDSR: Byte;
    var InvertDCD: Byte;
    var InvertRI: Byte;
    var Cbus0: Byte;
    var Cbus1: Byte;
    var Cbus2: Byte;
    var Cbus3: Byte;
    var Cbus4: Byte;
    var RIsD2XX: Byte;
    var PullDownEnable7: Byte;
    var SerNumEnable7: Byte;
    var ALSlowSlew: Byte;
    var ALSchmittInput: Byte;
    var ALDriveCurrent: Byte;
    var AHSlowSlew: Byte;
    var AHSchmittInput: Byte;
    var AHDriveCurrent: Byte;
    var BLSlowSlew: Byte;
    var BLSchmittInput: Byte;
    var BLDriveCurrent: Byte;
    var BHSlowSlew: Byte;
    var BHSchmittInput: Byte;
    var BHDriveCurrent: Byte;
    var IFAIsFifo7: Byte;
    var IFAIsFifoTar7: Byte;
    var IFAIsFastSer7: Byte;
    var AIsVCP7: Byte;
    var IFBIsFifo7: Byte;
    var IFBIsFifoTar7: Byte;
    var IFBIsFastSer7: Byte;
    var BIsVCP7: Byte;
    var PowerSaveEnable: Byte;
    var PullDownEnable8: Byte;
    var SerNumEnable8: Byte;
    var ASlowSlew: Byte;
    var ASchmittInput: Byte;
    var ADriveCurrent: Byte;
    var BSlowSlew: Byte;
    var BSchmittInput: Byte;
    var BDriveCurrent: Byte;
    var CSlowSlew: Byte;
    var CSchmittInput: Byte;
    var CDriveCurrent: Byte;
    var DSlowSlew: Byte;
    var DSchmittInput: Byte;
    var DDriveCurrent: Byte;
    var ARIIsTXDEN: Byte;
    var BRIIsTXDEN: Byte;
    var CRIIsTXDEN: Byte;
    var DRIIsTXDEN: Byte;
    var AIsVCP8: Byte;
    var BIsVCP8: Byte;
    var CIsVCP8: Byte;
    var DIsVCP8: Byte;
    var PullDownEnableH: Byte;
    var SerNumEnableH: Byte;
    var ACSlowSlewH: Byte;
    var ACSchmittInputH: Byte;
    var ACDriveCurrentH: Byte;
    var ADSlowSlewH: Byte;
    var ADSchmittInputH: Byte;
    var ADDriveCurrentH: Byte;
    var Cbus0H: Byte;
    var Cbus1H: Byte;
    var Cbus2H: Byte;
    var Cbus3H: Byte;
    var Cbus4H: Byte;
    var Cbus5H: Byte;
    var Cbus6H: Byte;
    var Cbus7H: Byte;
    var Cbus8H: Byte;
    var Cbus9H: Byte;
    var IsFifoH: Byte;
    var IsFifoTarH: Byte;
    var IsFastSerH: Byte;
    var IsFT1248H: Byte;
    var FT1248CpolH: Byte;
    var FT1248LsbH: Byte;
    var FT1248FlowControlH: Byte;
    var IsVCPH: Byte;
    var PowerSaveEnableH: Byte;
  end;

implementation

end.