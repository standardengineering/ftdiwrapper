//Copyright (C) 2008 by Jeroen Vandezande
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

namespace Essy.FTDIWrapper;

interface

type
  FTDIException = public class(Exception)
  private
  protected
  public
    property Status: FT_STATUS;
    constructor(aMessage: String; aStatus: FT_Status);
    constructor(aMessage: String);
  end;
  
  FTDIEnumerationException = public class(FTDIException)
  private
  protected
  public   
  end;
  
  FTDIDeviceException = public class(FTDIException)
  private
  protected
  public    
  end;

  FTDIDeviceLowLevelException = public class(FTDIDeviceException)
  private
  protected
  public    
  end;

  FTDIDeviceNotAllBytesTransmittedException = public class(FTDIDeviceException)
  private
  protected
  public    
  end;
  
implementation

constructor FTDIException(aMessage: String; aStatus: FT_Status); 
begin
  inherited constructor(aMessage + ' FT_STATUS : ' + aStatus.ToString);
  Self.Status := aStatus;
end;

constructor FTDIException(aMessage: String); 
begin
  inherited constructor(aMessage);
end;

end.